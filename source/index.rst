.. _contents: Table of contents


############
How to build
############

.. toctree::
   :maxdepth: 2

   howtobuild.rst
   rescue.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
