============
Requirements
============

The project is based on Yocto. Requirements for Yocto are set in Yocto's documentation,
check here:

::

        https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html

You need to install the following packages on a Debina or Debian-like Linux Build Host (other distros
are supported,too, please check in Yocto's documentation:)


::

        sudo apt install gawk wget git diffstat unzip texinfo gcc build-essential \
                chrpath socat cpio python3 python3-pip python3-pexpect xz-utils debianutils \
                iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev pylint3 \
                xterm python3-subunit mesa-common-dev zstd liblz4-tool

This project is based on Yocto with some additional layer. The configuration of the
layers is done using the tool `kas <https://github.com/siemens/kas>_` and the configuration file
is in YAML language. It contains the whole lists of layers with commit-ids. 

Install `kas` with:

::

        git clone https://github.com/siemens/kas.git
        cd kas.git
        sudo pip3 install .

============
How to build
============

To start building, just clone the following repo with:

::

        git clone https://gitlab.denx.de/controlhaus/kas-setup

You should have kas running on your host. You can choose if kas will run native or you can use
the kas container. More information on kas' Website.

You can ask kas to build everything (one line command), or to set a shell for you to work on.

.. _setup_environment:

Setting a OE shell
==================

You have to choose for which board you are building. There are two configuration files, one
for each hardware : controlhaus-bbg.yml and controlhaus-dhcor.yml


::

        kas shell kas-setup/controlhaus<your choice>.yml

Example:

::

        kas shell kas-setup/controlhaus-dhcor.yml

A *build* directory will be created and en environment is set. It makes sense to share
the directories with the downloaded packages and the *sstate* directory to all developers.
More info later.

::

        bitbake controlhaus-image

One command build
=================

::
        
        kas build kas-setup/controlhaus.yml

Build SDK
=========

The SDK is built according to the image. To create the SDK:

::

        bitbake -c populate_sdk controlhaus-image


The result are in the DEPLOY_DIR, that is tmp/deploy

::

        $ ls tmp/deploy/sdk/

        TODO : SDK controlhaus-glibc-x86_64-controlhaus-image-corei7-64-intel-corei7-64-toolchain-3.2.1.sh

To install the SDK, just run the sh file:

::

        tmp/deploy/sdk/controlhaus-glibc-x86_64-controlhaus-image-corei7-64-intel-corei7-64-toolchain-3.2.1.sh
        ControlHaus Yocto Distribution SDK installer version 3.2.1
        =======================================================
        Enter target directory for SDK (default: /opt/controlhaus/3.2.1): /opt/poky/controlhaus/3.2.1
        You are about to install the SDK to "/media/sdc1-1TB/poky/controlhaus/3.2.1". Proceed [Y/n]? Y
        [sudo] password for <your user>: 
        Extracting
        SDK...........................................
        ..............................................
        done Setting it up...
        done SDK has been successfully set up and is ready to be used.  Each time you wish to use
        the SDK in a new shell session, you need to source the environment setup script e.g.
        $ . /media/sdc1-1TB/poky/controlhaus/3.2.1/environment-setup-corei7-64-poky-linux
